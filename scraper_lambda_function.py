import requests
from bs4 import BeautifulSoup
import csv
import re
import time
import boto3
s3 = boto3.client('s3')

__results = []


def fetch(url):
    print('HTTP GET request to URL: %s' % url, end='')
    res = requests.get(url)
    print(' | Status code: %s' % res.status_code)

    return res


def parse(html):
    priceRegex = re.compile(r"[\£ ]+?(\d+([,\.\d]+)?)")
    soup = BeautifulSoup(html, "html.parser")

    cards = soup.findAll('a', {'data-testid': 'listing-details-link'})

    for card in cards:
        listing_spec = card.findAll("div", {"data-testid": "listing-spec"})
        for spec in listing_spec:
            if len(spec.text) == 0:
                bed, bath, reception = 0, 0, 0
            elif len(spec.text) == 1:
                bed, bath, reception = spec.text[0], 0, 0
            elif len(spec.text) == 2:
                bed, bath, reception = spec.text[0], spec.text[1], 0
            else:
                bed, bath, reception = spec.text[0], spec.text[1], spec.text[2]

        element = card.find('div', {'data-testid': 'listing-price'}).text

        try:
            price_string = priceRegex.search(element).group()
        except AttributeError:
            price_string = "POA"

        __results.append({
            'title': card.find('h2', {'data-testid': 'listing-title'}).text,
            'address': card.find('p', {'data-testid': 'listing-description'}).text,
            'price': price_string,
            'bed': bed,
            'bath': bath,
            'reception': reception
        })


def to_csv():

    with open('/tmp/lambda-temp.csv', 'w') as csv_file:
        writer = csv.DictWriter(
            csv_file, fieldnames=__results[0].keys())
        writer.writeheader()

        for row in __results:
            writer.writerow(row)

    with open('/tmp/lambda-temp.csv', 'rb') as data:
        s3.upload_fileobj(data, 'bs4-scraper-results123', 'house-info.csv')


def lambda_handler(event, context):
    for page in range(1, 142):
        url = "https://www.zoopla.co.uk/for-sale/property/london/e1/aldgate-stepney-mile-end-whitechapel/?property_sub_type"\
            "=flats&property_sub_type=semi_detached&property_sub_type=detached&property_sub_type=terraced&q=e1&radius=1&results_sort=newest_listings& search_source=refine&pn="
        url += str(page)
        res = fetch(url)
        parse(res.text)
        time.sleep(0.5)
    to_csv()

