
# Cloud Computing 7
<figure align = "center">
<img src="images/cloud7.png" alt="Trulli" width="800" height="423"">
</figure>


## Name
Lambda webscraper and Machine Learning prediction

## Description
Aims of project:
- Part 1: Carry out web scraping of zoopla for properties in E14 area of London.
- Part 2: Convert web scaper to Lambda function, to scrape and write data
- Part 3: Clean up and predict
- Part 4: Use machine learning to predict house price from user input   

## Results
<figure align = "center">
<img src="images/cloud3.png" alt="AWS " width="700" height="410"">
<figcaption align = "center"><b>AWS Diagram</b></figcaption>
</figure>



### Part 1 Local development
Scrape Zoopla with beautiful soup.  Convert to lambda and test locally.  Upload to AWS  
- `scaper_local.py` - web scraper ran locally without lambda
- `lambda_test` - folder containing web scraper tested locally as a docker image with lambda

### Part 2 AWS Lambda processing
- `scraper_lambda_fuction` - lambda function zipped with venv and uploaded as zip file to AWS.  This scrapes the Zoopla website and writes house data to S3.  Added inline policy to lambda execution role to allow put to S3 bucket.  Currently scrapes 375 pages or approx 10000 properties. 

### Part 3 Clean Up and Predict
- `PropertyExplore2.ipynb` - take scraping results, clean up and predict price for postcode, type of property, beds, baths and rection areas.

### Part 4 
- `lambda_function.py` - takes S3 data predicts and returns estimated price.  Currently triggered with idea of sms message input.  Lamda layers for scikit-learn and pandas installed.

## Results
<figure align = "center">
<img src="images/lambda2.png" alt="AWS " width="1144" height="359"">
<figcaption align = "center"><b>Execution results from text message test</b></figcaption>
</figure>

<br/><br/>

<figure align = "center">
<img src="images/lambda3.png" alt="AWS " width="1144" height="414"">
<figcaption align = "center"><b>Lambda Function info</b></figcaption>

<br/><br/>

</figure>
<figure align = "center">
<img src="images/lambda4.png" alt="AWS " width="1144" height="414"">
<figcaption align = "center"><b>Lambda Test text message</b></figcaption>
</figure>


### To do
- data generated from zoopla for one area E14 needs to be made more flexible location wise
- approx 9000 properties returned, dataset is small one with little variation in data, see notebooks for further info
- currently works from text message input, could work fully using Pinpoint
- set up scraper on schedule to get latest and more data



