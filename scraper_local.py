import requests
from bs4 import BeautifulSoup
import csv
import re
import json
import time


class ZooplaScraper:
    results = []

    def fetch(self, url):
        print('HTTP GET request to URL: %s' % url, end='')
        res = requests.get(url)
        print(' | Status code: %s' % res.status_code)

        return res

    def parse(self, html):
        priceRegex = re.compile(r"[\£ ]+?(\d+([,\.\d]+)?)")
        soup = BeautifulSoup(html, "html.parser")

        cards = soup.findAll('a', {'data-testid': 'listing-details-link'})

        for card in cards:
            listing_spec = card.findAll("div", {"data-testid": "listing-spec"})
            for spec in listing_spec:
                if len(spec.text) == 0:
                    bed, bath, reception = 0, 0, 0
                elif len(spec.text) == 1:
                    bed, bath, reception = spec.text[0], 0, 0
                elif len(spec.text) == 2:
                    bed, bath, reception = spec.text[0], spec.text[1], 0
                else:
                    bed, bath, reception = spec.text[0], spec.text[1], spec.text[2]

            element = card.find('div', {'data-testid': 'listing-price'}).text

            try:
                price_string = priceRegex.search(element).group()
            except AttributeError:
                price_string = "POA"

            self.results.append({
                'title': card.find('h2', {'data-testid': 'listing-title'}).text,
                'address': card.find('p', {'data-testid': 'listing-description'}).text,
                'price': price_string,
                'bed': bed,
                'bath': bath,
                'reception': reception
            })

    def to_csv(self):
        with open('house-e1.csv', 'w') as csv_file:
            writer = csv.DictWriter(
                csv_file, fieldnames=self.results[0].keys())
            writer.writeheader()

            for row in self.results:
                writer.writerow(row)

        print("Stored results to file")

    def run(self):
        for page in range(1, 142):
            url = "https://www.zoopla.co.uk/for-sale/property/london/e1/aldgate-stepney-mile-end-whitechapel/?property_sub_type"\
                "=flats&property_sub_type=semi_detached&property_sub_type=detached&property_sub_type=terraced&q=e1&radius=1&results_sort=newest_listings&search_source=refine&pn="
            url += str(page)
            res = self.fetch(url)
            self.parse(res.text)
            time.sleep(0.5)

        self.to_csv()


if __name__ == '__main__':
    scraper = ZooplaScraper()
    scraper.run()
