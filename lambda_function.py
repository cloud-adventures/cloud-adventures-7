import json
import pandas as pd
import numpy as np
import re
import boto3
import joblib
from io import BytesIO
from sklearn.tree import DecisionTreeRegressor
import warnings


def warn(*args, **kwargs):
    '''remove scikit learn warning'''
    pass


warnings.warn = warn

s3_client = boto3.client('s3')
s3_resource = boto3.resource('s3')

__regex = re.compile("[A-Z]{2}[0-9]{2}|[A-Z]{2}"
                     "[0-9]{1}|[A-Z]{1}[0-9]{2}|[A-Z]{1}[0-9]{1}")


def get_district_code(address):
    post = __regex.search(address)
    return post.group()


def clean_post_code(df):
    df['Postcode'] = df['address'].apply(get_district_code)
    df = df.groupby('Postcode').filter(lambda x: len(x) > 10)
    return df


def convert_price(price):
    price = price.replace("£", '').replace(",", "")
    return pd.to_numeric(price)


def get_property_type(title):
    property_types = ["semi-detached house", "terraced house", "flat", "detached house", "studio", "maisonette",
                      "terrace house", "bungalow", "cottage", "house", "lodge", "mobile"]
    property_var = ["terrace house", "semi detached house"]
    property_dict = {"terrace house": "terraced house",
                     "semi detached house": "semi-detached house"}

    for property_ in property_types:
        if property_ in title.lower():
            if property_ in property_var:
                return property_dict[property_].title().replace(" ", "")
            return property_.lower().replace("-", " ").title().replace(" ", "")
    return "Undefined"


def predict_price(X, model, location, proptype, bed, bath, reception):
    loc_index = np.where(X.columns == location)[0][0]
    loc_index2 = np.where(X.columns == proptype)[0][0]

    x = np.zeros(len(X.columns))
    x[0] = bed
    x[1] = bath
    x[2] = reception
    x[loc_index] = 1
    x[loc_index2] = 1

    return np.expm1(model.predict([x])[0])


def lambda_handler(event, context):

    message = json.loads(event['Records'][0]['Sns']['Message'])
    postcode = message['postcode']
    proptype = message['proptype']
    beds = message['beds']
    bath = message['bath']
    reception = message['reception']

    try:
        bucket_name = "bs4-scraper-results123"
        s3_file_name = "house-e1.csv"
        resp = s3_client.get_object(Bucket=bucket_name, Key=s3_file_name)
        df = pd.read_csv(resp['Body'], sep=',')

    except Exception as err:
        print(err)

    df = clean_post_code(df)

    df.drop(index=df[df['price'] == "POA"].index, inplace=True)
    df['Price'] = df['price'].apply(convert_price)
    df['Type'] = df['title'].apply(get_property_type)

    df["Price"] = np.log1p(df["Price"])

    df = df.rename(
        columns={"bed": "Bed", "bath": "Bath", "reception": "Reception"})

    df = df.drop(['title', 'address', 'price'], axis=1)

    df = pd.get_dummies(df, columns=['Type', 'Postcode'], prefix_sep='')

    X = df.drop('Price', axis='columns')
    y = df.Price

    with BytesIO() as data:
        s3_resource.Bucket(bucket_name).download_fileobj('model3.pkl', data)
        model = joblib.load(data)

    price = int(predict_price(X, model, postcode,
                proptype, beds, bath, reception))
    price = "£{:,}".format(price)

    return {"Price is": price}
