import requests
from bs4 import BeautifulSoup
import csv
import re
import json
import time

__results = []


def fetch(url):
    print('HTTP GET request to URL: %s' % url, end='')
    res = requests.get(url)
    print(' | Status code: %s' % res.status_code)

    return res


def parse(html):
    global __results
    priceRegex = re.compile(r"[\£ ]+?(\d+([,\.\d]+)?)")
    soup = BeautifulSoup(html, "html.parser")

    cards = soup.findAll('a', {'data-testid': 'listing-details-link'})

    for card in cards:
        listing_spec = card.findAll("div", {"data-testid": "listing-spec"})
        for spec in listing_spec:
            if len(spec.text) != 3:
                bed, bath, reception = None, None, None
            else:
                bed, bath, reception = spec.text[0], spec.text[1], spec.text[2]

        element = card.find('div', {'data-testid': 'listing-price'}).text
        price_string = priceRegex.search(element).group()

        __results.append({
            'title': card.find('h2', {'data-testid': 'listing-title'}).text,
            'address': card.find('p', {'data-testid': 'listing-description'}).text,
            'price': price_string[1:],
            'bed': bed,
            'bath': bath,
            'reception': reception
        })
        print(__results)


def to_csv():
    with open('zoopla.csv', 'w') as csv_file:
        writer = csv.DictWriter(
            csv_file, fieldnames=__results[0].keys())
        writer.writeheader()

        for row in __results:
            writer.writerow(row)

    print('Stored results to csv file')


def handler(event, context):
    for page in range(1, 2):
        url = 'http://www.zoopla.co.uk/for-sale/property/london/?identifier=london&q=London&search_source=for-sale&radius=0&pn='
        url += str(page)
        res = fetch(url)
        parse(res.text)
        time.sleep(1)
    # to_csv()


# if __name__ == '__main__':

#    lambda_handler()
    #scraper = ZooplaScraper()
    # scraper.run()
